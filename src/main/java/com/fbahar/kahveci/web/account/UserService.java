package com.fbahar.kahveci.web.account;

import java.util.Collections;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import com.fbahar.kahveci.web.offering.Offering;
import com.fbahar.kahveci.web.offering.OfferingRepository;

public class UserService implements UserDetailsService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private OfferingRepository offeringRepository;

	@PostConstruct
	protected void initialize() {
		accountRepository.save(new Account("user", "user", "ROLE_USER"));
		accountRepository.save(new Account("admin", "admin", "ROLE_ADMIN"));

		offeringRepository.upsert(new Offering(null, "Filtre kahve", "ilk veri", false, true, 4));
		offeringRepository.upsert(new Offering(null, "Latte", "ilk veri", false, true, 5));
		offeringRepository.upsert(new Offering(null, "Mocha", "ilk veri", false, true, 6));
		offeringRepository.upsert(new Offering(null, "Çay", "ilk veri", false, true, 3));

		offeringRepository.upsert(new Offering(null, "Süt", "ilk veri", true, false, 2));
		offeringRepository.upsert(new Offering(null, "Fındık Şurubu", "ilk veri", true, false, 3));
		offeringRepository.upsert(new Offering(null, "Çikolata sosu", "ilk veri", true, false, 5));
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountRepository.findByEmail(username);
		if (account == null) {
			throw new UsernameNotFoundException("user not found");
		}
		return createUser(account);
	}

	public void signin(Account account) {
		SecurityContextHolder.getContext().setAuthentication(authenticate(account));
	}

	private Authentication authenticate(Account account) {
		return new UsernamePasswordAuthenticationToken(createUser(account), null,
				Collections.singleton(createAuthority(account)));
	}

	private User createUser(Account account) {
		return new User(account.getEmail(), account.getPassword(), Collections.singleton(createAuthority(account)));
	}

	private GrantedAuthority createAuthority(Account account) {
		return new SimpleGrantedAuthority(account.getRole());
	}

}
