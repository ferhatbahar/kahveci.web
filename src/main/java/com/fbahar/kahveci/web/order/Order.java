package com.fbahar.kahveci.web.order;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fbahar.kahveci.web.account.Account;

@SuppressWarnings("serial")
@Entity
@Table(name = "Order_tb")
public class Order implements Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account_fk")
	private Account createdBy;

	private Date createdAt;

	private String status;

	private Long mainOfferingId;

	private String mainOfferingName;

	private int mainOfferingPrice;

	@OneToMany(orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "ORDER_UNIT_ID")
	private Set<OrderAddon> addons;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Account getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Account createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getMainOfferingId() {
		return mainOfferingId;
	}

	public void setMainOfferingId(Long mainOfferingId) {
		this.mainOfferingId = mainOfferingId;
	}

	public String getMainOfferingName() {
		return mainOfferingName;
	}

	public void setMainOfferingName(String mainOfferingName) {
		this.mainOfferingName = mainOfferingName;
	}

	public int getMainOfferingPrice() {
		return mainOfferingPrice;
	}

	public void setMainOfferingPrice(int mainOfferingPrice) {
		this.mainOfferingPrice = mainOfferingPrice;
	}

	public Set<OrderAddon> getAddons() {
		return addons;
	}

	public void setAddons(Set<OrderAddon> addons) {
		this.addons = addons;
	}

	public OrderSummary toSummary() {

		int total = this.addons.stream().mapToInt(o -> o.getAddonOfferingCount() * o.getAddonOfferingPrice()).sum()
				+ this.mainOfferingPrice;

		StringBuffer name = new StringBuffer();
		this.addons.stream().forEach(o -> {
			name.append("{" + o.getAddonOfferingCount() + " X " + o.getAddonOfferingName() + "} ");
		});


		return new OrderSummary(this.id, this.createdBy.getEmail(), this.createdAt, this.mainOfferingName,
				name.toString(), total);

	}

}
