package com.fbahar.kahveci.web.order;

import java.util.Date;

public class OrderSummary {
	private long orderId;
	private String createdBy;
	private Date createdAt;
	private String mainOffering;
	private String addonOfferings;
	private int totalPrice;

	public OrderSummary() {
	}

	public OrderSummary(long orderId, String createdBy, Date createdAt, String mainOffering, String addonOfferings,
			int totalPrice) {
		super();
		this.orderId = orderId;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.mainOffering = mainOffering;
		this.addonOfferings = addonOfferings;
		this.totalPrice = totalPrice;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getMainOffering() {
		return mainOffering;
	}

	public void setMainOffering(String mainOffering) {
		this.mainOffering = mainOffering;
	}

	public String getAddonOfferings() {
		return addonOfferings;
	}

	public void setAddonOfferings(String addonOfferings) {
		this.addonOfferings = addonOfferings;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

}
