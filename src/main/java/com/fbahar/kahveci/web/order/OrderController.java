package com.fbahar.kahveci.web.order;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fbahar.kahveci.web.account.Account;
import com.fbahar.kahveci.web.account.AccountRepository;
import com.fbahar.kahveci.web.offering.Offering;
import com.fbahar.kahveci.web.offering.OfferingForm;
import com.fbahar.kahveci.web.offering.OfferingRepository;
import com.fbahar.kahveci.web.support.web.MessageHelper;

@Controller
public class OrderController {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	OfferingRepository offeringRepository;

	@Autowired
	ObjectMapper objectMapper;

	/**
	 * siparis liste sayfasi
	 * 
	 * @param principal
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public String index(Principal principal, Model model) {

		Account account = accountRepository.findByEmail(principal.getName());

		model.addAttribute("orders",
				"ROLE_USER".equals(account.getRole()) ? orderRepository.getOrderSummariesByAccountId(account.getId())
						: "ROLE_ADMIN".equals(account.getRole()) ? orderRepository.getAllOrderSummaries() : null);

		return "order/listorder";
	}

	/**
	 * yeni siparis sayfasi
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/order/new", method = RequestMethod.GET)
	public String newOrder(Model model) {

		model.addAttribute("mainOfferings", offeringRepository.getMainOfferings());
		model.addAttribute("addonOfferings", offeringRepository.getAddonOfferings());
		model.addAttribute("addonCounts", new int[] { 1, 2, 3, 4, 5, 6 });
		model.addAttribute(new OrderForm());

		return "order/order";
	}

	/**
	 * siparis kayit
	 * 
	 * @param principal
	 * @param form
	 * @param errors
	 * @param ra
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/order/save", method = RequestMethod.POST)
	public String save(Principal principal, @Valid @ModelAttribute OrderForm form, Errors errors, RedirectAttributes ra)
			throws JsonParseException, JsonMappingException, IOException {
		if (errors.hasErrors()) {
			return "/order/order";
		}

		Account account = accountRepository.findByEmail(principal.getName());

		/**
		 * addonlar onyuzden jsonstr olarak geliyor, form submit icin en kolay
		 * yontem bu gorundu
		 */
		List<AddonForm> addons = objectMapper.readValue(form.getAddonInfoList(), new TypeReference<List<AddonForm>>() {
		});

		/**
		 * yeni siparis olusurken aktif kullanici uzerine bu anın tarihi ile
		 * kaydedilir
		 * 
		 */
		Offering moff = offeringRepository.getOffering(form.getMainOfferingId());

		Order order = new Order();
		order.setCreatedBy(account);
		order.setCreatedAt(new Date());

		order.setMainOfferingId(moff.getId());
		order.setMainOfferingName(moff.getName());
		order.setMainOfferingPrice(moff.getPrice());
		order.setAddons(new HashSet<OrderAddon>());
		addons.stream().forEach(af -> {

			Offering addonOffering = offeringRepository.getOffering(af.getId());
			OrderAddon orderAddon = new OrderAddon();
			orderAddon.setAddonOfferingId(addonOffering.getId());
			orderAddon.setAddonOfferingName(addonOffering.getName());
			orderAddon.setAddonOfferingPrice(addonOffering.getPrice());
			orderAddon.setAddonOfferingCount(af.getCount());

			order.getAddons().add(orderAddon);

		});

		orderRepository.save(order);

		MessageHelper.addSuccessAttribute(ra, "neworder.success");

		return "redirect:/order";
	}
}
