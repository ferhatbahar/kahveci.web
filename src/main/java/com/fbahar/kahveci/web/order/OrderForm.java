package com.fbahar.kahveci.web.order;

public class OrderForm {
	
	private long id;

	private long mainOfferingId;
	
	private String addonInfoList;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public long getMainOfferingId() {
		return mainOfferingId;
	}

	public void setMainOfferingId(long mainOfferingId) {
		this.mainOfferingId = mainOfferingId;
	}

	public String getAddonInfoList() {
		return addonInfoList;
	}

	public void setAddonInfoList(String addonInfoList) {
		this.addonInfoList = addonInfoList;
	}
	

	
	
}
