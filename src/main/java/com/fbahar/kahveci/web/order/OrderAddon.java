package com.fbahar.kahveci.web.order;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "OrderAddon_tb")
public class OrderAddon implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	
	private long addonOfferingId;

	private String addonOfferingName;
	
	private int addonOfferingPrice;
	
	private int addonOfferingCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getAddonOfferingId() {
		return addonOfferingId;
	}

	public void setAddonOfferingId(long addonOfferingId) {
		this.addonOfferingId = addonOfferingId;
	}

	public String getAddonOfferingName() {
		return addonOfferingName;
	}

	public void setAddonOfferingName(String addonOfferingName) {
		this.addonOfferingName = addonOfferingName;
	}

	public int getAddonOfferingPrice() {
		return addonOfferingPrice;
	}

	public void setAddonOfferingPrice(int addonOfferingPrice) {
		this.addonOfferingPrice = addonOfferingPrice;
	}

	public int getAddonOfferingCount() {
		return addonOfferingCount;
	}

	public void setAddonOfferingCount(int addonOfferingCount) {
		this.addonOfferingCount = addonOfferingCount;
	}
	
	
}
