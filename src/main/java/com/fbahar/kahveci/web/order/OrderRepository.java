package com.fbahar.kahveci.web.order;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class OrderRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Order> getAllOrders() {
		return this.entityManager.createQuery("from Order").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<OrderSummary> getOrderSummariesByAccountId(long accountId) {
		List<Order> orders = this.entityManager.createQuery("from Order where createdBy.id=:accId")
				.setParameter("accId", accountId).getResultList();

		return orders.stream().map(o -> o.toSummary()).collect(Collectors.toList());
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<OrderSummary> getAllOrderSummaries() {
		List<Order> orders = this.entityManager.createQuery("from Order").getResultList();

		return orders.stream().map(o -> o.toSummary()).collect(Collectors.toList());
	}

	@Transactional
	public void save(Order order) {

		this.entityManager.persist(order);

		if (order.getAddons() != null) {
			order.getAddons().forEach(a -> this.entityManager.persist(a));
		}

	}

}
