package com.fbahar.kahveci.web.home;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fbahar.kahveci.web.account.Account;
import com.fbahar.kahveci.web.account.AccountRepository;

@Controller
public class HomeController {

	@Autowired
	AccountRepository accountRepository;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Principal principal) {

		if (principal == null)
			return "home/homeNotSignedIn";

		Account account = accountRepository.findByEmail(principal.getName());

		return "ROLE_USER".equals(account.getRole()) ? "home/homeUser"
				: "ROLE_ADMIN".equals(account.getRole()) ? "home/homeAdmin" : "home/homeSignedIn";

	}
}

