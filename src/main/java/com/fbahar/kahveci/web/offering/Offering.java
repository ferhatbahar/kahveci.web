package com.fbahar.kahveci.web.offering;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.mapping.Set;

@SuppressWarnings("serial")
@Entity
@Table(name = "offering")
public class Offering implements java.io.Serializable {
	
	@Id
	@GeneratedValue
	private Long id;

	private String name;

	private String description;
	
	private boolean isAddon;
	
	private boolean isMain;
	
	private int price;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isAddon() {
		return isAddon;
	}

	public void setAddon(boolean isAddon) {
		this.isAddon = isAddon;
	}

	public boolean isMain() {
		return isMain;
	}

	public void setMain(boolean isMain) {
		this.isMain = isMain;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Offering(Long id, String name, String description, boolean isAddon, boolean isMain, int price) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.isAddon = isAddon;
		this.isMain = isMain;
		this.price = price;
	}

	public Offering() {
		super();
	}

	
}
