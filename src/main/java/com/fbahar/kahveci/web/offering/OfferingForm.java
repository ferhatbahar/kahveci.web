package com.fbahar.kahveci.web.offering;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

public class OfferingForm {

	private long id;

	@NotBlank(message = "{newoffering.nameempty}")
	private String name;

	@NotBlank(message = "{newoffering.descriptionempty}")
	private String description;

	@Min(1)
	private int price;

	private String isMain;

	private String isAddon;

	public String getIsMain() {
		return isMain;
	}

	public void setIsMain(String isMain) {
		this.isMain = isMain;
	}

	public String getIsAddon() {
		return isAddon;
	}

	public void setIsAddon(String isAddon) {
		this.isAddon = isAddon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Offering assignToOffering(Offering offering) {

		if (id > 0)
			offering.setId(id);

		offering.setName(name);
		offering.setDescription(description);
		offering.setPrice(price);
		offering.setAddon("true".equals(isAddon));
		offering.setMain("true".equals(isMain));

		return offering;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public static OfferingForm createForm(Offering offering) {
		OfferingForm form = new OfferingForm();
		form.setId(offering.getId());
		form.setName(offering.getName());
		form.setDescription(offering.getDescription());
		form.setIsAddon(offering.isAddon() ? "true" : "");
		form.setIsMain(offering.isMain() ? "true" : "");
		form.setPrice(offering.getPrice());

		return form;
	}

}
