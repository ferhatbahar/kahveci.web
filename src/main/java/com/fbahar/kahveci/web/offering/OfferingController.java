package com.fbahar.kahveci.web.offering;



import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fbahar.kahveci.web.support.web.MessageHelper;

@Controller
public class OfferingController {

	Logger logger = LoggerFactory.getLogger(OfferingController.class);

	@Autowired
	OfferingRepository offeringRepository;

	/**
	 * listemele sayfasi
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/offering", method = RequestMethod.GET)
	public String index(Model model) {

		model.addAttribute("offerings", offeringRepository.getAllOfferings());

		return "offering/listoffering";
	}

	/**
	 * yeni offering sayfası
	 * @param model
	 * @return
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/offering/new", method = RequestMethod.GET)
	public String newOffering(Model model) {
		model.addAttribute(new OfferingForm());
		return "offering/offering";
	}

	/**
	 * offering guncelleme sayfasi
	 * @param model
	 * @param id
	 * @return
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/offering/{offeringId}/edit", method = RequestMethod.GET)
	public String editOffering(Model model, @PathVariable("offeringId") int id) {

		Offering offering = offeringRepository.getOffering(id);

		model.addAttribute(OfferingForm.createForm(offering));
		return "offering/offering";
	}

	/**
	 * offering kaydet formu
	 * @param form
	 * @param errors
	 * @param ra
	 * @return
	 */
	@RequestMapping(value = "/offering/save", method = RequestMethod.POST)
	public String save(@Valid @ModelAttribute OfferingForm form, Errors errors, RedirectAttributes ra) {

		logger.info("offering save starting");

		if (errors.hasErrors()) {
			return "/offering/offering";
		}

		Offering offering = form.assignToOffering(new Offering());

		offeringRepository.upsert(offering);

		MessageHelper.addSuccessAttribute(ra, "newoffering.success");

		return "redirect:/offering";
	}
}
