package com.fbahar.kahveci.web.offering;

import java.util.List;

import javax.management.RuntimeErrorException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
@Secured ("ROLE_ADMIN")
public class OfferingRepository {

	Logger logger = LoggerFactory.getLogger("com.fbahar.kahveci.web");
	
	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	@SuppressWarnings("rawtypes")
	public List getAllOfferings() {
		
		List l= this.entityManager.createQuery("from Offering").getResultList();
		
		logger.debug("result count {}",l.size());
		
		return l;
		
	}

	@Transactional
	public Offering getOffering(long id) {
		return (Offering) this.entityManager.createQuery("from Offering where id=:id").setParameter("id", id)
				.getSingleResult();
	}

	/**
	 * offeringi guncelle ya da yeni olustur
	 * @param offering eger id sifirdan buyukse var olan bir offering yoksa yeni offering olarak degerlendiriliyor.
	 */
	@Transactional
	public void upsert(Offering offering){

			logger.debug("new upsert" );
			Offering _offering = (offering.getId() != null && offering.getId() > 0) ? this.entityManager.merge(offering)
					: offering;

			this.entityManager.persist(_offering);

	}
	

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Offering> getAddonOfferings() {
		return this.entityManager.createQuery("from Offering where isAddon=:p1").setParameter("p1", Boolean.TRUE)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Offering> getMainOfferings() {
		return this.entityManager.createQuery("from Offering where isMain=:p1").setParameter("p1", Boolean.TRUE)
				.getResultList();
	}

}
